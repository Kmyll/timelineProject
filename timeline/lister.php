<?php
session_start();

if(array_key_exists('user', $_SESSION) == false) {
  header('Location: index.php');
  exit();
}else{
  include "header.php";
  include "footer.php";
}
include 'config/connection.php';

$query = $pdo->prepare('SELECT * FROM projectsadmin ORDER BY `group`');

$executeIsOk = $query->execute();

//récupération des résultats

$projets = $query->fetchAll();

//Dictionaries for entities

$array = [
  0 => "EFE",
  1 => "CFPJ",
  2 => "ISM",
  3 => "ACP",
  4 => "PYRAMYD",
  5 => "IDCC",
  6 => "IFE",
  7 => "CFPJ",
  8 => "UE",
  9 => "ABW Digital",
  10 => "Transverse",
  11 => "Matrice",
  12 => "eBasicc",
  13 => "Espace Client",
  14 => "CRM",
  15 => "GOA Catalogue",
  16 => "DSI",
  17 => "SRC",
  18 => "Emargement électronique",
  19 => "BI",
  20 => "Base Intervenants"
];

?>

<h1 class="admin-title"><i class="fas fa-list-ul"></i> Liste des contacts</h1>
<table class="list-project-table">
  <thead>
    <tr class="lister-tr">
      <td class="head entite">Entité</td>
      <td class="head intitule">Intitulé</td>
      <td class="head description">Description</td>
      <td class="head debut">Début</td>
      <td class="head fin">Fin prévue</td>
      <td class="head progression">Progression (%)</td>
      <td class="head statut">Statut</td>
      <td class="head modifier">Modifier</td>
      <td class="head supprimer">Supprimer</td>
    </tr>
  </thead>
  <tbody>
    <?php foreach($projets as $projet): ?>
      <tr class="lister-tr">
        <td class="tbrow entite tblalg"><?=$array[$projet['group']]?></td>
        <td class="tbrow description"><?=$projet['content']?></td>
        <td class="intitule tbrow"><?=$projet['title']?></td>
        <td class="debut tbrow tblalg"><?=$projet['start']?> </td>
        <td class="fin tbrow tblalg"><?=$projet['end']?> </td>
        <td class="progression tbrow tblalg"><?= ($projet['Progression'] * 100) . '%'?></td>
        <td class="statut tbrow"><?=$projet['className']?></td>
        <td class="tblalg"><a href="modify.php?modifyProject=<?=$projet['id']?>"><i class=" sbmt fas fa-pencil-alt"></i></a> </td>
        <td class="tblalg"><a href="delete.php?deleteProject=<?=$projet['id']?>"><i class="sbmt fas fa-trash-alt"></i></a> </td>

      </tr>
    </tbody>
  <?php endforeach;?>
</table>
