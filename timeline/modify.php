<?php
session_start();

if(array_key_exists('user', $_SESSION) == false) {
  header('Location: index.php');
  exit();
}else{
  include "header.php";
  include "footer.php";
}

include 'config/connection.php';

$query = $pdo->prepare('SELECT * FROM projectsadmin WHERE id=:id');
$query->bindValue(':id', $_GET['modifyProject'], PDO::PARAM_INT);

$executeIsOk = $query->execute();

$projet = $query->fetch();
?>


<section class="timeline-form add">

  <h1 class="admin-title"><i class="fas fa-pen"></i> Modifier un projet</h1>
  <section class="inside-form-admin">
    <form class="form main-form modify-form" action="modifyBack.php" method="POST">
      <fieldset class="inside-form-fieldset">
        <input class="input inpmdf" type="hidden" name="modifyProject" value="<?=$projet['id'] ?>">
        <p class="p">
          <label class="label" for="entity">Entité</label>
          <select class="select mdfslct" class="select-entity" name="group"  required value="<?= $projet['group'];?>">
            <option value="0"<?= $projet['group'] == '0' ?  "selected" :  ""; ?>>EFE</option>
            <option value="1"<?= $projet['group'] == '1' ?  "selected" :  "";   ?>>CFPJ</option>
            <option value="2"<?= $projet['group'] == '2' ?  "selected" :  "";   ?>>ISM</option>
            <option value="3"<?= $projet['group'] == '3' ?  "selected" :  "";   ?>>ACP</option>
            <option value="4"<?= $projet['group'] == '4' ?  "selected" :  "";   ?>>PYRAMYD</option>
            <option value="5"<?= $projet['group'] == '5' ?  "selected" :  "";   ?>>IDCC</option>
            <option value="6"<?= $projet['group'] == '6' ?  "selected" :  "";   ?>>IFE</option>
            <option value="7"<?= $projet['group'] == '7' ?  "selected" :  "";   ?>>UE</option>
            <option value="8"<?= $projet['group'] == '8' ?  "selected" :  "";   ?>>ABW Digital</option>
            <option value="9"<?= $projet['group'] == '9' ?  "selected" :  "";   ?>>Transverse</option>
            <option value="10"<?= $projet['group'] == '10' ?  "selected" :  "";   ?>>Matrice</option>
            <option value="11"<?= $projet['group'] == '11' ?  "selected" :  "";   ?>>eBasicc</option>
            <option value="12"<?= $projet['group'] == '12' ?  "selected" :  "";   ?>>Espace client</option>
            <option value="13"<?= $projet['group'] == '13' ?  "selected" :  "";   ?>>CRM</option>
            <option value="14"<?= $projet['group'] == '14' ?  "selected" :  "";   ?>>GOA - Catalogue</option>
            <option value="15"<?= $projet['group'] == '15' ?  "selected" :  "";   ?>>DSI</option>
            <option value="16"<?= $projet['group'] == '16' ?  "selected" :  "";   ?>>SRC</option>
            <option value="17"<?= $projet['group'] == '17' ?  "selected" :  "";   ?>>Emargement électronique</option>
            <option value="18"<?= $projet['group'] == '18' ?  "selected" :  "";   ?>>BI</option>
            <option value="19"<?= $projet['group'] == '19' ?  "selected" :  "";   ?>>Base intervenants</option>
          </select> <br><br>
        </p>
        <p class="p">
          <label class="label" for="project">Intitulé du projet</label>
          <input class="input inpmdf" type="text" name="content" required value="<?= $projet['content']; ?>"><br><br>
        </p>
        <p class="p">
          <label class="label" for="">Description</label>
          <input class="input inpmdf" class="input class="input"" type="text" name="title" required value="<?= $projet['title']; ?>"><br><br>
        </p>
        <p class="p">
          <label class="label" for="">Date de début</label>
          <input class="input inpmdf startDate" type="date" name="start" required value="<?= $projet['start']; ?>"><br><br>
        </p>
        <p class="p">
          <label class="label" for="">Date de fin</label>
          <input class="input inpmdf endDate" type="date" name="end" required value="<?= $projet['end']; ?>"><br><br>
        </p>
        <p class="p">
          <label class="label" for="">Pourcentage de progression</label>
          <select class="select mdfslct" class="" name="Progression" required value="<?= $projet['Progression']; ?>">
            <option value="0" <?= $projet['Progression'] == '0' ?  "selected" :  "";?>>0%</option>
            <option value="0.05"<?= $projet['Progression'] == '0.05' ?  "selected" :  "";?>>5%</option>
            <option value="0.10"<?= $projet['Progression'] == '0.10' ?  "selected" :  "";?>>10%</option>
            <option value="0.15"<?= $projet['Progression'] == '0.15' ?  "selected" :  "";?>>15%</option>
            <option value="0.20"<?= $projet['Progression'] == '0.20' ?  "selected" :  "";?>>20%</option>
            <option value="0.25"<?= $projet['Progression'] == '0.25' ?  "selected" :  "";?>>25%</option>
            <option value="0.30"<?= $projet['Progression'] == '0.30' ?  "selected" :  "";?>>30%</option>
            <option value="0.35"<?= $projet['Progression'] == '0.35' ?  "selected" :  "";?>>35%</option>
            <option value="0.40"<?= $projet['Progression'] == '0.40' ?  "selected" :  "";?>>40%</option>
            <option value="0.45"<?= $projet['Progression'] == '0.45' ?  "selected" :  "";?>>45%</option>
            <option value="0.50"<?= $projet['Progression'] == '0.50' ?  "selected" :  "";?>>50%</option>
            <option value="0.54"<?= $projet['Progression'] == '0.55' ?  "selected" :  "";?>>55%</option>
            <option value="0.60"<?= $projet['Progression'] == '0.60' ?  "selected" :  "";?>>60%</option>
            <option value="0.65"<?= $projet['Progression'] == '0.65' ?  "selected" :  "";?>>65%</option>
            <option value="0.70"<?= $projet['Progression'] == '0.70' ?  "selected" :  "";?>>70%</option>
            <option value="0.75"<?= $projet['Progression'] == '0.75' ?  "selected" :  "";?>>75%</option>
            <option value="0.80"<?= $projet['Progression'] == '0.80' ?  "selected" :  "";?>>80%</option>
            <option value="0.85"<?= $projet['Progression'] == '0.85' ?  "selected" :  "";?>>85%</option>
            <option value="0.90"<?= $projet['Progression'] == '0.90' ?  "selected" :  "";?>>90%</option>
            <option value="0.95"<?= $projet['Progression'] == '0.95' ?  "selected" :  "";?>>95%</option>
            <option value="1"<?= $projet['Progression'] == '1' ?  "selected" :  "";?>>100%</option>
          </select><br><br>
        </p>
        <p class="p">
          <label class="label" for="">Statut</label>
          <select class="select mdfslct statut" class="" name="className" required value="<?= $projet['className']; ?>">
            <option value="Non-démarré" <?= $projet['className'] == 'Non-démarré' ?  "selected" :  "";   ?>>Non démarré</option>
            <option value="En-cours"<?= $projet['className'] == 'En-cours' ?  "selected" :  "";   ?>>En cours</option>
            <option value="En-retard"<?= $projet['className'] == 'En-retard' ?  "selected" :  "";   ?>>En retard</option>
            <option value="Livré"<?= $projet['className'] == 'Livré' ?  "selected" :  "";   ?>>Livré</option>
            <option value="Alerte"<?= $projet['className'] == 'Alerte' ?  "selected" :  "";   ?>>Alerte</option>
          </select>
        </p>
      </fieldset>
      <section class="add-cta">
        <button class="submit" type="Submit" name="Submit">Modifier</button>
        <a href="index.php"><button class="cancel" type="button" name="button">Annuler</button></a>
      </section>
    </form>
