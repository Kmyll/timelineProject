<?php
session_start();

if($_POST['start'] > $_POST['end']){
  include "header.php";
  ?>
  <p class="error"><i class="fas fa-exclamation-triangle"></i> <br>
    La date de deadline ne peut pas être avant la date de début du projet.
    <br>Merci de réessayer.</p>
  <?php

  echo "<script>setTimeout(\"location.href = 'admin.php';\",2000);</script>";
  exit();
};

if(empty($_POST) == false){
  //  var_dump($_POST);
  include 'config/connection.php';
}

$query = $pdo->prepare(
'INSERT INTO
projectsadmin
(`group`, `content`, `title`, `start`, `end`, `Progression`, `className`)
VALUES
(?, ?, ?, ?, ?, ?, ?)'
);

$query->execute(
[
  htmlspecialchars($_POST['group']),
  htmlspecialchars($_POST['content']),
  htmlspecialchars($_POST['title']),
  htmlspecialchars($_POST['start']),
  htmlspecialchars($_POST['end']),
  htmlspecialchars($_POST['Progression']),
  htmlspecialchars($_POST['className'])
]);

?>
<?php include "header.php"; ?>
<h1><i class="fas fa-check"></i> Le projet a bien été créé</h1><br><br>
<section class="add-cta">
  <a href="index.php"><button class="submit" type="button" name="button">Retourner sur la timeline</button></a>
  <a href="admin.php"><button class="submit" type="button" name="button">Ajouter un autre projet</button></a>
</section>
<?php include "footer.php";?>
