<?php session_start();
include "header.php";
include "footer.php";?>


<h1 class="conection-title"><i class=" login-man fas fa-user-edit"></i> Connexion</h1>

<section class="connection-page">
  <article class="abilways-img">
    <img class="bys" src="img/bys.jpg" alt="Abilways - Build your singularity">
  </article>
  <article class="connection-form">
    <p class="connect-explaination">Merci d'entrer votre identifiant et votre mot de passe pour accéder à votre espace personel.</p>
    <p class="connect-explaination2"> <span class="red-star">*</span> Si vous ne parvenez pas à vous connecter, merci de contacter votre administrateur</p><br>
    <form class="register-form" action="connectBack.php" method="post">
      <input class="register-front" type="text" name="Email" value="" placeholder="Identifiant" required> <br>
      <input class="register-front" type="password" name="Password" value="" placeholder="Mot de passe" required> <br>
      <button class="register-btn" type="submit" name="button"><i class="powerbtn fas fa-power-off"></i></button>
    </form>
  </article>
</section>

<?php include "footer.php" ?>
