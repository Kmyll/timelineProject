<?php
	$pdo = new PDO
	(
		"mysql:host=46.105.14.216;port=8443;dbname=timeline;charset=utf8",
		"timeline_user",
		"cz92e#X5",
	    [
	    	PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
	      PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
	    ]
    );

/* check connection */
if ($pdo->connect_errno) {
    printf("Connect failed: %s\n", $pdo->connect_error);
    exit();
}

/* check if server is alive */
if ($pdo->ping()) {
    printf ("Our connection is ok!\n");
} else {
    printf ("Error: %s\n", $pdo->error);
}

/* close connection */
$pdo->close();
?>
