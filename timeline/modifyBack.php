<?php

session_start();
if($_POST['start'] > $_POST['end']){
  include "header.php";
  ?>
  <p class="error"><i class="fas fa-exclamation-triangle"></i> <br>
    La date de deadline ne peut pas être avant la date de début du projet.
    <br>Merci de réessayer.</p>
  <?php

  echo "<script>setTimeout(\"location.href = 'admin.php';\",2000);</script>";
  exit();
};

if(array_key_exists('user', $_SESSION) == false) {
  header('Location: index.php');
  exit();
}

include 'config/connection.php';

$query = $pdo->prepare(
'UPDATE projectsadmin
SET title =:title, start=:start, end=:end, content=:content, Progression=:Progression, className=:className, `group`=:group
WHERE id=:id'
);

$query->bindValue(':id', ($_POST['modifyProject']), PDO::PARAM_INT);
$query->bindValue(':title', ($_POST['title']), PDO::PARAM_STR);
$query->bindValue(':start', ($_POST['start']), PDO::PARAM_STR);
$query->bindValue(':end', ($_POST['end']), PDO::PARAM_STR);
$query->bindValue(':content', ($_POST['content']), PDO::PARAM_STR);
$query->bindValue(':Progression', ($_POST['Progression']), PDO::PARAM_STR);
$query->bindValue(':className', ($_POST['className']), PDO::PARAM_STR);
$query->bindValue(':group', ($_POST['group']), PDO::PARAM_STR);


$executeIsOk = $query->execute();

header('Location: index.php');
exit();
