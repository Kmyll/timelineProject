<?php
session_start();

include 'config/connection.php';
include 'header.php';
include "footer.php";
include "today.php";

?>
<main id='main'>

  <section class="main-nav">
    <?php
    if(array_key_exists('user', $_SESSION) == false) { ?>
      <a href="connexion.php"><button class="login-btn"><i class=" login-man fas fa-user-edit"></i></button></a>
    <?php } else {?>
      <section class="ifonline">
        <p class="greet">Bonjour <b><?= $_SESSION['user']['Email']?></b>, Vous êtes connecté. Que voulez-vous faire aujourd'hui?</p>
        <section class="logged">
          <a class="log-link" href="admin.php"><button class="add-btn" type="button" name="button"><i class="fas fa-plus"></i></button></a>
          <a class="log-link" href="lister.php"><button class="del-btn" type="button" name="button"><i class="fas fa-exchange-alt"></i></button></a>
          <a class="log-link" href="logout.php"><button class="logout-btn" type="button" name="button"><i class="fas fa-sign-out-alt"></i></button></a>
        </section>
      </section>
    </section>
  <?php } ?> <br>

  <h1 class="main-title">Timeline des Projets de la DSI</h1>
  <section class="container">
    <section class="intro-flex">
      <section class="legend-section">
        <article class="legend">
          <h3 class="legend-title">Légende des couleurs de la timeline</h3>
        </article>

        <article class="legendCore">
          <i class="blue fas fa-circle"> <span class="legendspan">Non démarré</span></i>
          <i class="orange fas fa-circle"> <span class="legendspan"> En cours</span></i>
          <i class="white fas fa-circle"> <span class="legendspan">En retard</span></i>
          <i class="green fas fa-circle"> <span class="legendspan"> Terminé</span></i>
          <i class="red fas fa-circle"> <span class="legendspan"> Alerte</span></i>
        </article>
      </section>

      <section class="btn-section">
        <h3 class="legend-title">Boutons directionnels</h3>
        <article class="btnCore">
          <button  class="btn" id="zoomIn" value="Zoom in" type="button" name="button"><i class="btndir fas fa-search-plus"></i></button>
          <button class="btn" id="zoomOut" value="Zoom out" type="button" name="button"><i class="btndir fas fa-search-minus"></i></button>
          <button class="btn" id="moveLeft" value="moveLeft" type="button" name="button"><i class="btndir far fa-arrow-alt-circle-left"></i></button>
          <button class="btn" id="moveRight" value="moveLeft" type="button" name="button"><i class="btndir far fa-arrow-alt-circle-right"></i></button>
          <button class="btn" id="fit" value="Fit all items" type="button" name="button"><i class="btndir fas fa-align-center"></i></button>
          <button class="btn" id="moveTo" value="Move to today"type="button" name="button"><i class="btndir far fa-calendar-check"></i></button>
        </article>
      </section>
    </section>

    <!--ENCODAGE DE PHP MY SQL AU FORMAT JSON-->
    <textarea id="data">
      <?php
      $connect = mysqli_connect("https://46.105.14.216:8443", "timeline_user", "cz92e#X5", "timeline");
      $connect->set_charset("utf8");
      $sql = "SELECT * FROM projectsadmin";
      $result = mysqli_query($connect, $sql);
      $json_array = array();
      while($row = mysqli_fetch_assoc($result))
      {
        if( DateTime::createFromFormat('Y-m-d', $row["end"]) <= $now && $row["Progression"] != "1")
          $row["className"] = "Alerte";
        $json_array[] = $row;
      }

      echo(json_encode($json_array));
      ?>


    </textarea>



    <div class="buttons">
      <input type="button" id="load" value="&darr; Load" title="Load data from textarea into the Timeline">
      <input type="button" id="save" value="&uarr; Save" title="Save data from the Timeline into the textarea">
    </div>

    <!--- TIMELINE ----------------------------------------------------->
    <div id="visualization"></div>
  </section>
</main>

<?php
include 'footer.php';
?>
