'use strict'

var now = moment().minutes(0).seconds(0).milliseconds(0);

var groups = new vis.DataSet();
groups.add([
  {id: 0, content: 'EFE', value: 0},
  {id: 1, content: 'CFPJ', value: 1},
  {id: 2, content: 'ISM', value: 2},
  {id: 3, content: 'ACP', value: 3},
  {id: 4, content: 'PYRAMYD', value: 4},
  {id: 5, content: 'IDCC', value: 5},
  {id: 6, content: 'IFE', value: 6},
  {id: 7, content: 'UE', value: 7},
  {id: 8, content: 'ABW Digital', value: 8},
  {id: 9, content: 'Transverse', value: 9, nestedGroups: [10,11,12,13,14,15,16,17,18,19]}
]);
groups.add([
  //Modifier les nested group en accord
  {id: 10, content: 'Matrice', value: 10},
  {id: 11, content: 'eBasicc', value: 11},
  {id: 12, content: 'Espace Client', value: 12},
  {id: 13, content: 'CRM', value: 13},
  {id: 14, content: 'GOA Catalogue', value: 14},
  {id: 15, content: 'DSI', value: 15},
  {id: 16, content: 'SRC', value: 16},
  {id: 17, content: 'Emargement electronique', value: 17},
  {id: 18, content: 'BI', value: 18},
  {id: 19, content: 'Base Intervenants', value: 19}
]);

var txtData = document.getElementById('data');
var btnLoad = document.getElementById('load');
var btnSave = document.getElementById('save');
var items = new vis.DataSet();
var container = document.getElementById('visualization');

var options = {
  //min: new Date(2017, 0, 1),
  max: new Date(2025, 0, 1),
  groupOrder: function (a, b){
    return a.value - b.value;
  },
  editable: true,
  showCurrentTime: true,
  clickToUse: true,
  start: now,
  visibleFrameTemplate: function(item) {
    if (item.visibleFrameTemplate) {
      return item.visibleFrameTemplate;
    }
    var percentage = item.Progression * 100 + '%';
    return '<div class="progress-wrapper"><div class="progress" style="width:' + percentage + '"></div><label class="progress-label">' + percentage + '<label></div>';
  }
};

var timeline = new vis.Timeline(container, items, options);
timeline.setOptions(options);
timeline.setGroups(groups);
timeline.setItems(items);

function loadData () {
  var data = JSON.parse(txtData.value);
  items.clear();
  items.add(data);
  timeline.fit();
  //  timeline.setItems(items);
}
btnLoad.onclick = loadData;

function saveData() {
  var data = items.get({
    type: {
      start: 'ISODate',
      end: 'ISODate'
    }
  });
  // serialize the data and put it in the textarea
  txtData.value = JSON.stringify(data, null, 2);
}
btnSave.onclick = saveData;

// load the initial data
loadData();

function move (percentage) {
  var range = timeline.getWindow();
  var interval = range.end - range.start;
  timeline.setWindow({
    start: range.start.valueOf() - interval * percentage,
    end:   range.end.valueOf()   - interval * percentage
  });
}

/********* Buttons ********/

document.getElementById('fit').onclick = function (){timeline.fit();};
document.getElementById('moveTo').onclick = function (){ timeline.moveTo(moment());};
document.getElementById('zoomIn').onclick = function (){ timeline.zoomIn( 0.2); };
document.getElementById('zoomOut').onclick = function () { timeline.zoomOut( 0.2); };
document.getElementById('moveLeft').onclick = function () { move( 0.2); };
document.getElementById('moveRight').onclick = function () { move(-0.2); };

/****** Axis orientation ********/

    timeline.setOptions({ orientation: {axis: "both"} });
// We can use "bottom", "both", "none" or "top" to change its position
