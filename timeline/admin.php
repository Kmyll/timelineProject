<?php
session_start();

if(array_key_exists('user', $_SESSION) == false) {
  header('Location: index.php');
  exit();
}else{
  include "header.php";
  include "footer.php";
}
?>

<section class="timeline-form add">

  <h1 class="admin-title"><i class="fas fa-plus"></i> Ajouter un projet</h1>
  <section class="inside-form-admin">
    <form class="main-form" action="adminBack.php" method="POST">
      <fieldset class="inside-form-fieldset">
        <p class="p">
          <label class="label" for="entity">Entité</label class="label">
            <select class="select" class="select-entity"  required name="group">
              <option selected value=""></option>
              <option value="0">EFE</option>
              <option value="1">CFPJ</option>
              <option value="2">ISM</option>
              <option value="3">ACP</option>
              <option value="4">PYRAMYD</option>
              <option value="5">IDCC</option>
              <option value="6">IFE</option>
              <option value="7">UE</option>
              <option value="8">ABW Digital</option>
              <option value="9">Transverse</option>
              <option value="10">Matrice</option>
              <option value="11">eBasicc</option>
              <option value="12">Espace client</option>
              <option value="13">CRM</option>
              <option value="14">GOA - Catalogue</option>
              <option value="15">DSI</option>
              <option value="16">SRC</option>
              <option value="17">Emargement électronique</option>
              <option value="18">BI</option>
              <option value="19">Base intervenants</option>
            </select><br><br>
          </p>
          <p class="p">
            <label class="label" for="project">Intitulé du projet</label class="label">
              <input class="input" type="text" name="content"  required value=""><br><br>
            </p>
            <p class="p">
              <label class="label" for="">Description</label class="label">
                <textarea  class="textarea" name="title" rows="4" cols="80"required value=""></textarea><br><br>
              </p>
              <p class="p">
                <label class="label" for="">Date de début</label class="label">
                  <input class="input startDate" type="date" name="start" required value=""><br><br>
                </p>
                <p class="p">
                  <label class="label" for="">Date de fin</label class="label">
                    <input class="input endDate" type="date" name="end" required value=""><br><br>
                  </p>


                  <p class="p">
                    <label class="label" for="">Pourcentage de progression</label class="label">
                      <select class="select" class="" required name="Progression">
                        <option selected value=""></option>
                        <option value="0">0%</option>
                        <option value="0.05">5%</option>
                        <option value="0.10">10%</option>
                        <option value="0.15">15%</option>
                        <option value="0.20">20%</option>
                        <option value="0.25">25%</option>
                        <option value="0.30">30%</option>
                        <option value="0.35">35%</option>
                        <option value="0.40">40%</option>
                        <option value="0.45">45%</option>
                        <option value="0.50">50%</option>
                        <option value="0.55">55%</option>
                        <option value="0.60">60%</option>
                        <option value="0.65">65%</option>
                        <option value="0.70">70%</option>
                        <option value="0.75">75%</option>
                        <option value="0.80">80%</option>
                        <option value="0.85">85%</option>
                        <option value="0.90">90%</option>
                        <option value="0.95">95%</option>
                        <option value="1">100%</option>
                      </select><br><br>
                    </p>
                      <p class="p">
                        <label class="label" for="">Statut</label class="label">
                          <select class="select" class="" required name="className">
                            <option selected value=""></option>
                            <option value="Non-démarré">Non démarré</option>
                            <option value="En-cours">En cours</option>
                            <option value="En-retard">En retard</option>
                            <option value="Livré">Livré</option>
                            <option value="Alerte">Alerte</option>
                          </select>
                        </p>
                      </fieldset>
                      <section class="add-cta">
                        <button class="submit" type="Submit" name="Submit">Créer</button>
                        <a href="index.php"><button class="cancel" type="button" name="button">Annuler</button></a>
                      </section>
                    </form>
                  </section>
                </section>




                <?php include "footer.php" ?>
