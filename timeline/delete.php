<?php
session_start();
include "header.php";
include "footer.php";
if(array_key_exists('user', $_SESSION) == false) {
  header('Location: index.php');
  exit();
}

include 'config/connection.php';

$query = $pdo->prepare('DELETE FROM projectsadmin WHERE id=:id LIMIT 1');
$query->bindValue(':id', $_GET['deleteProject'], PDO::PARAM_INT);

$executeIsOk = $query->execute();

if($executeIsOk){
  $message ='le contact a été supprimé';
}else{
  $message ='échec';
}
?>
<h1><i class="fas fa-times"></i> Le projet a bien été supprimé</h1><br><br>
<section class="add-cta">
  <a href="index.php"><button class="submit" type="button" name="button">Retourner à la timeline</button></a>
  <a href="lister.php"><button class="submit" type="button" name="button">Retourner à la liste des projets</button></a>
</section>
